package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Integer getSize();

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Project project);

    Task removeById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task removeByIndex(Integer index);

}
