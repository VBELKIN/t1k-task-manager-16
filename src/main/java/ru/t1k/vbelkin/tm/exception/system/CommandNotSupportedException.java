package ru.t1k.vbelkin.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error!Argument not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error!Command `` " + command + " `` not supported...");
    }

}
