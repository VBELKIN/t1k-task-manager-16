package ru.t1k.vbelkin.tm.exception.field;

public class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! ProjectId is empty...");
    }

}
